
function getQueryVariable(variable) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  } 
}

function generate_nodes_edges({nodes_n= 100, edges_n=500, labels='random'}={}){
	var i,
			s,
			o,
			N = nodes_n,
			E = edges_n,
			C = 5,
			d = 0.5,
			cs = [],
			g = {
				nodes: [],
				edges: []
			};

	for (i = 0; i < C; i++)
		cs.push({
			id: i,
			nodes: [],
			color: '#' + (
				Math.floor(Math.random() * 16777215).toString(16) + '000000'
			).substr(0, 6)
		});

	for (i = 0; i < N; i++) {
		o = cs[(Math.random() * C) | 0];
		g.nodes.push({
			id: 'n' + i,
			label: labels,
			x: 100 * Math.cos(2 * i * Math.PI / N),
			y: 100 * Math.sin(2 * i * Math.PI / N),
			size: Math.random(),
			color: window.defined_color
		});
		o.nodes.push('n' + i);
	}

	for (i = 0; i < E; i++) {
		if (Math.random() < 1 - d)
			g.edges.push({
				id: 'e' + i,
				source: 'n' + ((Math.random() * N) | 0),
				target: 'n' + ((Math.random() * N) | 0)
			});
		else {
			o = cs[(Math.random() * C) | 0]
			g.edges.push({
				id: 'e' + i,
				source: o.nodes[(Math.random() * o.nodes.length) | 0],
				target: o.nodes[(Math.random() * o.nodes.length) | 0]
			});
		}
	}

	return g;
}



function onChangeSlider(){
	let nodes_value = document.getElementById("nodes_n").value;
	let edges_value = document.getElementById("edges_n").value;
	window.graphElement.killForceAtlas2();
	window.g = generate_nodes_edges({nodes_n: nodes_value, edges_n: edges_value, labels: window.graph_name});
	window.graphElement.graph.clear();
	window.graphElement.graph.read({nodes: window.g.nodes, edges: window.g.edges});
	window.graphElement.refresh();
	window.graphElement.startForceAtlas2({worker: true, barnesHutOptimize: false});

}

function toogleEdges(){
	if(this.checked) {
      window.graphElement.settings('drawEdges', true);
			window.graphElement.refresh();
    } else {
      window.graphElement.settings('drawEdges', false);
			window.graphElement.refresh();
    }

	
}

function changeNodesColor(){
	 window.defined_color = '#' + (Math.floor(Math.random() * 16777215).toString(16) + '000000').substr(0, 6);
	window.graphElement.graph.nodes().forEach(function(node){
            node.color = window.defined_color;
        });
	window.graphElement.refresh();
}

function changeBackgroundColor() {
	var background_color = '#' + (Math.floor(Math.random() * 16777215).toString(16) + '000000').substr(0, 6);
   document.body.style.background = background_color;
}


function main(){

	//Get title from url parameter
	window.graph_name = getQueryVariable('name') ? getQueryVariable('name') : 'random' ;
	document.getElementById("graph_name").innerHTML = window.graph_name;

	//Set  nodes default color to black
	window.defined_color = '#000000';

	//Initialize Random Graph
	window.g = generate_nodes_edges({labels: window.graph_name});
	window.graphElement = new sigma({
		graph: window.g,
		container: 'graph-container',
		settings: {
			drawEdges: true
		}
	});
	window.graphElement.startForceAtlas2({worker: true, barnesHutOptimize: false});


	//Associate Inputs with functions
	let slider_nodes = document.getElementById('nodes_n');
	let slider_edges = document.getElementById('edges_n');
	let toogle_edges = document.getElementById('toogle_edges');

	slider_nodes.addEventListener('change', onChangeSlider);
	slider_edges.addEventListener('change', onChangeSlider);
	toogle_edges.addEventListener('change', toogleEdges);

}

main()