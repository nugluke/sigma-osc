var OscApp = OscApp || {};

(function () {
    "use strict";

    var nodes_n;
    var edges_n;
    var toogle_edges;
    var accept_events;
    var event_time_control = false;
    var interval_time = 5000;

    //Toogle Accept or Reject Events
    setInterval(function(){ 
    	if(accept_events){
    		accept_events = false;
    	} else {
    		accept_events = true;
    	}
     }, interval_time);


    var valueTransform = function (value) {
        return (value * 5000) + 20;
    };



    OscApp.SocketGraph = function () {
        this.oscPort = new osc.WebSocketPort({
            url: "ws://localhost:8081"
        });

        this.listen();
        this.oscPort.open();

        this.oscPort.socket.onmessage = function (e) {
            //console.log("message", e);
        };

    };

    OscApp.SocketGraph.prototype.listen = function () {
        
        this.oscPort.on("message", this.mapMessage.bind(this));
        this.oscPort.on("message", function (msg) {
            console.log("message", msg);
        });
    };

    OscApp.SocketGraph.prototype.onChangeOsc = function (){

        window.graphElement.killForceAtlas2();
        window.g = generate_nodes_edges({nodes_n: nodes_n, edges_n: edges_n, labels: window.graph_name});
        window.graphElement.graph.clear();
        window.graphElement.graph.read({nodes: window.g.nodes, edges: window.g.edges});
        window.graphElement.refresh();
        window.graphElement.startForceAtlas2({worker: true, barnesHutOptimize: false});

}

	OscApp.SocketGraph.prototype.toogleEdges = function (){
	if(toogle_edges) {
      window.graphElement.settings('drawEdges', true);
			window.graphElement.refresh();
    } else {
      window.graphElement.settings('drawEdges', false);
			window.graphElement.refresh();
    }

	
}
    OscApp.SocketGraph.prototype.mapMessage = function (oscMessage) {
    	var address = oscMessage.address;
	    var value = oscMessage.args[0];

    	if(!event_time_control || accept_events){
	        var transformed_value = valueTransform(value);

	        if(address == '/oscControl/slider1'){
	            nodes_n = transformed_value;
	            this.onChangeOsc();

	        }
	        if(address == '/oscControl/slider2'){
	        		edges_n = valueTransform(value);
	            	this.onChangeOsc();
	        }
    	}

    	if(address == '/oscControl/toggle1'){
	        		toogle_edges = value;
	            	this.toogleEdges();
	    }

    	if(address == '/oscControl/toggle2'){
    				if(value != 0){
	            		changeNodesColor();
	            	}
	    }
	        if(address == '/oscControl/toggle3'){
	        		if(value != 0){
	            	changeBackgroundColor();
	            	}
	    }

        

    };

}());
