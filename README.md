# Sigma.js + OSC.js

Visualização utilizando Sigma.js e Osc.js

![Example](https://media.giphy.com/media/VDNDVGIbmsSPeSQ8sh/giphy.gif)


Paramêtros controláveis implementados até o momento
<table>
    <tr>
        <th>Endereço OSC</th>
        <th>Tipo de paramêtro</th>
        <th>Paramtero da Visualização</th>
    </tr>
    <tr>
        <td>/oscControl/slider1</td>
        <td>float 0.0 - 1.0</td>
        <td>Numero de Nós do Grafo</td>
    </tr>
    <tr>
        <td>/oscControl/slider2</td>
        <td>float 0.0 - 1.0</td>
        <td>Numero de Arestas do Grafo</td>
    </tr>
    <tr>
        <td>/oscControl/toggle1</td>
        <td>bool 0 or 1</td>
        <td>Mostrar ou não arestas do grafo</td>
    </tr>
    <tr>
        <td>/oscControl/toggle2</td>
        <td>Pulse</td>
        <td>Muda a cor dos nós do grafo</td>
    </tr>
    <tr>
        <td>/oscControl/toggle3</td>
        <td>Pulse</td>
        <td>Muda a cor do fundo</td>
    </tr>
</table>

## Instalação

1. Instale o [Node.js](https://nodejs.org/en/)
2. Acesse a pasta via terminal e execute <code>npm install</code> para instalar as dependencias Node.

## Rodando

1. Execute <code>node .</code> no terminal
2. Acesse <code>http://localhost:8081</code> no seu browser
